UI::Font@ segmentFont =  UI::LoadFont("fonts/Sixteen-Mono.otf", 48.f);

// array<Alarm@> alarms;
array<Clock@> clocks;
// array<Stopwatch@> stopwatches;
// array<Timer@> timers;

void RenderMenu()
{
    if (UI::BeginMenu(Plugin::Title)) {
        if (UI::MenuItem("\\$0d5" + Icons::ClockO + " \\$zAlarm", "", false, false)) {
            print("");
        }
        if (UI::MenuItem("\\$fe0" + Icons::ClockO + " \\$zClock", "")) {
            Clock::New(clocks);
        }
        if (UI::MenuItem("\\$0cd" + Icons::ClockO + " \\$zStopwatch", "", false, false)) {
            print("");
        }
        if (UI::MenuItem("\\$c16" + Icons::ClockO + " \\$zTimer", "", false, false)) {
            print("");
        }
        if (clocks.Length > 0) {
            UI::Separator();
            for (uint i = 0; i < clocks.Length; i++) {
                auto clock = clocks[i];
                if (UI::MenuItem(clock.TitleID, "", clock.visible)) {
                    clock.visible = !clock.visible;
                }
            }
        }
        UI::EndMenu();
    }
}

void Render()
{
    if (clocks.Length > 0) {
        for (uint i = 0; i < clocks.Length; i++) {
            auto clock = clocks[i];
            clock.Render();
            if (!clock.open)
                clocks.RemoveAt(i--);
        }
    }
}

void Update(float dt)
{
    if (clocks.Length > 0) {
        for (uint i = 0; i < clocks.Length; i++) {
            clocks[i].Update(dt);
        }
    }
}

void Main()
{
    Clock::New(clocks);
}