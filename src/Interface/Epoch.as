final class Epoch
{
    int64 time = 0;

	int64 offsetH = 0;
    int64 offsetM = 0;
    int64 offsetS = 0;

    string format = "%H:%M:%S";

    Epoch(const int64 &in _time)
    {
        time = _time;
    }
}