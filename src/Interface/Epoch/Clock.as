namespace Clock // Interface?
{
	void New(array<Clock@>@ list)
	{
		uint id = CalculateID(list);
        list.InsertAt(id, Clock(id, list));
	}

	uint CalculateID(array<Clock@>@ list)
    {
        uint length = list.Length;
        for (uint i = 0; i < length; i++)
            if (i != list[i].id)
                return i;
        return length;
    }
}

final class Clock : Window
{
    string get_Label() override { return "Clock"; }
    string get_Color() override { return "\\$fe0"; }

    string get_ID() { return " #" + (id + 1); }
	string get_TitleID() { return Title + ID; }

	WindowProperties@ properties;

	Epoch@ epoch;
	array<Clock@>@ all;

    Clock(uint _id, array<Clock@>@ _all)
	{
		id = _id;
        @all = _all;
		@epoch = Epoch(Time::Stamp);
		@properties = WindowProperties();
		tabs.InsertLast(Format(epoch));
		tabs.InsertLast(Offset(epoch));
		tabs.InsertLast(Colour(properties));
	}

    void Render()
	{
		if (!visible) {
			return;
		}

		string display = Time::FormatString(epoch.format, epoch.time);
		string displayBack = ";; ;; ;;"; // calculate according to display string
		vec2 displaySize = Draw::MeasureString(display, segmentFont, 48.f);
		vec2 displayAlign;

		uint windowFlags = UI::WindowFlags::NoTitleBar + UI::WindowFlags::AlwaysUseWindowPadding;
		uint styleColorStack = 0;
		uint styleVarStack = 0;

		UI::PushStyleColor(UI::Col::WindowBg, vec4(properties.colorBG.x, properties.colorBG.y, properties.colorBG.z, properties.opacityBG));
		styleColorStack++;

		if (!UI::IsOverlayShown()) {
			windowFlags += UI::WindowFlags::NoInputs;
			displayAlign = vec2((size.x - displaySize.x) / 2, (size.y - displaySize.y) / 2);
		} else {
			windowFlags += UI::WindowFlags::MenuBar;
			displayAlign = vec2((size.x - displaySize.x) / 2, (size.y - displaySize.y + Style::MenuBarSize) / 2);
			UI::PushStyleColor(UI::Col::MenuBarBg, focused ? vec4(.18f, .18f, .18f, 1) : vec4(.04f, .04f, .04f, 1));
			UI::PushStyleColor(UI::Col::Border, focused ? vec4(.18f, .18f, .18f, 1) : vec4(.04f, .04f, .04f, 1));
			styleColorStack += 2;
		}

		if (initial) {
        	UI::SetNextWindowPos(Draw::GetWidth() / 2, Draw::GetHeight() / 2, UI::Cond::Appearing, 0.5f, 0.5f);
			UI::SetNextWindowSize(int(displaySize.x + 64), int(displaySize.y + 64)); // 64 is padding
			initial = false;
		}
		
		if (UI::Begin(TitleID, visible, windowFlags)) {

			UI::PushID(ID);

			UpdateWindowData();
			RenderTitleBar(TitleID);

			UI::PushFont(segmentFont);

			UI::PushStyleColor(UI::Col::Text, vec4(properties.colorBD.x, properties.colorBD.y, properties.colorBD.z, properties.opacityBD));
			UI::SetCursorPos(displayAlign);
            UI::Text(displayBack);
			UI::PopStyleColor();

			UI::PushStyleColor(UI::Col::Text, vec4(properties.colorFG.x, properties.colorFG.y, properties.colorFG.z, properties.opacityFG));
			UI::SetCursorPos(displayAlign);
            UI::Text(display);
			UI::PopStyleColor();

			UI::PopFont();

			if (UI::IsOverlayShown()) {
				SetVisibilityOffset(false);
				for (uint i = 0; i < tabs.Length; i++) {
					auto tab = tabs[i];
					if (tab.selected) {
						UI::Dummy(vec2((size.x - displaySize.x) / 2, (size.y - displaySize.y - Style::MenuBarSize) / 2));
						UI::Separator();
						tab.Render();
					}
				}
			} else {
				SetVisibilityOffset(true);
				CloseCurrentTab();
			}

			UI::PopID();
		}

		UI::End();

		UI::PopStyleVar(styleVarStack);
		UI::PopStyleColor(styleColorStack);
	}

	void Update(float dt)
	{
		if (visible) {
			epoch.time = Time::Stamp + (3600 * epoch.offsetH) + (60 * epoch.offsetM) + epoch.offsetS;
		}
	}
}