final class Colour : Tab
{
    string get_Label() override { return "Color"; }

    WindowProperties@ properties;

    Colour(WindowProperties@ _properties)
    {
        @properties = _properties;
    }

    void Render() override
    {
        UI::Markdown("__Color__");
        properties.colorFG = UI::InputColor3("Foreground", properties.colorFG);
		properties.colorBG = UI::InputColor3("Background", properties.colorBG);
        properties.colorBD = UI::InputColor3("Backdrop", properties.colorBD);
        UI::Markdown("__Opacity__");
        properties.opacityFG = UI::SliderFloat("Foreground", properties.opacityFG, 0, 1);
		properties.opacityBG = UI::SliderFloat("Background", properties.opacityBG, 0, 1);
        properties.opacityBD = UI::SliderFloat("Backdrop", properties.opacityBD, 0, 1);
    }
}