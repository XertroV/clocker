final class Format : Tab
{
    string get_Label() override { return "Format"; }

    Epoch@ epoch;

    Format(Epoch@ _epoch)
    {
        @epoch = _epoch;
    }

    void Render() override
    {
        epoch.format = UI::InputText("Format", epoch.format);
    }
}