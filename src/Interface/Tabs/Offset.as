final class Offset : Tab
{
    string get_Label() override { return "Offset"; }

    Epoch@ epoch;

    Offset(Epoch@ _epoch)
    {
        @epoch = _epoch;
    }

    void Render() override
    {
        epoch.offsetH = UI::InputInt("Hours", epoch.offsetH, 1);
        epoch.offsetM = UI::InputInt("Minutes", epoch.offsetM, 1);
        epoch.offsetS = UI::InputInt("Seconds", epoch.offsetS, 1);
    }
}