namespace Style
{
    const uint MenuBarSize = 24;
    const vec2 ItemSpacing = vec2(10, 6);
    const vec2 IconsSize = vec2(18, 18);
}

abstract class Window : Info
{
    uint id;

    vec2 size;
    vec2 position;

	array<Tab@> tabs;
    Tab@ currentTab;

    array<bool> buttons;

    bool open = true;
	bool visible = true;
	bool initial = true;
    bool focused = true;

    private bool offset = false;

    void CloseCurrentTab()
    {
        if (currentTab !is null) {
            currentTab.selected = false;
            @currentTab = null;
        }
    }

    void SetVisibilityOffset(bool _offset)
    {
        if (offset == _offset) return;
        size.y = _offset ? size.y - Style::MenuBarSize : size.y + Style::MenuBarSize;
        position.y = _offset ? position.y + Style::MenuBarSize : position.y - Style::MenuBarSize;
        UI::SetWindowSize(size);
        UI::SetWindowPos(position);
        offset = _offset;
    }

    void RenderTitleBar(string title)
	{
		if (UI::IsOverlayShown()) {
			UI::BeginMenuBar();

			UI::Text(title);

			for (uint i = 0; i < tabs.Length; i++) {
				auto tab = tabs[i];
				if (UI::MenuItem(tab.Label, "", tab.selected)) {
					if (!tab.selected) {
						tab.selected = true;
						if (currentTab !is null) {
							currentTab.selected = false;
						}
						@currentTab = tab;
					} else {
						currentTab.selected = false;
						@currentTab = null;
					}
				}
			}

			RenderTitleBarButtons();

			UI::EndMenuBar();
		}
	}

    void RenderTitleBarButtons()
    {
        const vec2 windowSize = UI::GetWindowSize();
        const vec2 cursorPos = UI::GetCursorPos();
        for (uint i = 1; i <= 2; i++) {
            UI::SetCursorPos(vec2(windowSize.x - ((Style::IconsSize.x + (Style::ItemSpacing.x * 2)) * i), cursorPos.y));
            switch (i) {
                case 1: if (UI::MenuItem(Icons::WindowCloseO)) open = false; break;
                case 2: if (UI::MenuItem(Icons::WindowMinimize)) visible = false; break;
            }
        }
    }

    void UpdateWindowData()
    {
        position = UI::GetWindowPos();
		size = UI::GetWindowSize();
        focused = UI::IsWindowFocused();
    }
}