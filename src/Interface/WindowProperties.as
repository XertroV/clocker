final class WindowProperties
{
	vec3 colorFG = Colours::White;
	vec3 colorBG = Colours::Black;
	vec3 colorBD = Colours::Black;

	float opacityFG = 1;
	float opacityBG = .5f;
	float opacityBD = .1f;

    WindowProperties() { }
}