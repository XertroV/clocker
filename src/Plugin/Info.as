namespace Plugin
{
    string get_Label() { return "Clocker"; }
    string get_Icon() { return Icons::ClockO; }
    string get_Color() { return "\\$fa0"; }
    string get_Title() { return Color + Icon + " \\$z" + Label; }
}

namespace Colours
{
    vec3 get_White() { return vec3(1, 1, 1); }
    vec3 get_Black() { return vec3(0, 0, 0); }
}

abstract class Info
{
    string get_Label() { return Plugin::Label; }
    string get_Icon() { return Plugin::Icon; }
    string get_Color() { return Plugin::Color; }
    string get_Title() { return Color + Icon + " \\$z" + Label; }
}
